import java.util.Scanner;
public class v10_valores {
	private static Scanner entrada;
	public static void main(String[] args) {
		
		System.out.println("");
		System.out.println("Programa que permita a leitura de 10 valores (inteiros)");
		System.out.println("");
		int N =10;
		entrada = new Scanner(System.in);
		int[] numeros = new int[N];
		for (int i=0; i<N; i++) {
			System.out.println(i + "� Numero: ");
			numeros[i] = entrada.nextInt();
			
		}
	/*	System.out.println("1� Numero: ");
		numeros[0] = entrada.nextInt();
		System.out.println("2� Numero: ");
		numeros[1] = entrada.nextInt();
		System.out.println("3� Numero: ");
		numeros[2] = entrada.nextInt();
		System.out.println("4� Numero: ");
		numeros[3] = entrada.nextInt();
		System.out.println("5� Numero: ");
		numeros[4] = entrada.nextInt();
		System.out.println("6� Numero: ");
		numeros[5] = entrada.nextInt();
		System.out.println("7� Numero: ");
		numeros[6] = entrada.nextInt();
		System.out.println("8� Numero: ");
		numeros[7] = entrada.nextInt();
		System.out.println("9� Numero: ");
		numeros[8] = entrada.nextInt();
		System.out.println("10� Numero: ");
		numeros[9] = entrada.nextInt();
		*/

		double max = Double.NEGATIVE_INFINITY;
		for (int i=0; i<10; i++) {
			if (numeros[i] > max) {
				max = numeros[i];
			}
		}

		double min = Double.POSITIVE_INFINITY;
		for (int i=0; i<10; i++) {
			if (numeros[i] < min) {
				min = numeros[i];
			}
		}

		int pares = 0;
		for (int i = 1; i<10; i++) {
			if (numeros[i] % 2 == 0) {
				pares++;
			}
		}

		int impares = numeros.length - pares;

		System.out.println("O maior numero e "+max+" e o menor e: "+min);
		System.out.println("A diferen�a entre maximo e minimo no vetor � de "+(max-min));
		System.out.println("H� "+pares+" numeros pares e "+impares+" numeros impares");
		
	}

}
