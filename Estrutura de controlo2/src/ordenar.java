import java.util.Scanner;

public class ordenar {
	private static Scanner entrada;
	public static void main(String[] args) {
		
		System.out.println("");
		System.out.println("Programa que ordene um vetor de N elementos.");
		System.out.println("");

		entrada = new Scanner(System.in);
		int[] numeros = new int[5];
		System.out.println("1� Numero: ");
		numeros[0] = entrada.nextInt();
		System.out.println("2� Numero: ");
		numeros[1] = entrada.nextInt();
		System.out.println("3� Numero: ");
		numeros[2] = entrada.nextInt();
		System.out.println("4� Numero: ");
		numeros[3] = entrada.nextInt();
				
		for (int i = 0 ; i<5; i++) {
			for (int j = i+1; j<5; j++) {
				if (numeros[j] < numeros[i]){
					int temp = numeros[j];
					numeros [j] = numeros [i];
					numeros [i] = temp;
				}
 			}
		}
		System.out.println("");
		for (int i = 0; i < 5; i++) {
			System.out.println(numeros[i]);
			}
		
	}
}